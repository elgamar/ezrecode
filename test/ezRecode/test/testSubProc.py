#!/usr/env python

import subprocess
import shlex
import re

proc = subprocess.Popen( shlex.split("mencoder pippo.avi -oac copy -ovc copy -o pippo2.avi"), bufsize=0, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)

#Error handling
IOErrMatch = re.compile( r"^Cannot open file/device." )
InFmtErrMatch = re.compile( r"^============ Sorry, this file format is not recognized/supported =============" )
ParseErrMatch = re.compile( r"^Error parsing option on the command line:(.*?)" )

#Progress and final stats handling
progressMatch = re.compile( r"^Pos:\s*(\d+.\d?)s\s*(\d+?)f \(\s?(\d+)%\)\s*(\d+.\d+)fps Trem:\s*(\d+)min\s*(\d+)mb.*?A-V:.*?\[(\d+):(\d+)\](.*?)" )
endVideoStatsMatch = re.compile( r"^Video stream:\s*(\d+.\d+) kbit/s.*?size:\s*(\d+)\s*bytes\s*(\d+.\d+) secs.*?" )
endAudioStatsMatch = re.compile( r"^Audio stream:\s*(\d+.\d+) kbit/s.*?size:\s*(\d+)\s*bytes\s*(\d+.\d+) secs.*?" )

while True:
  line = proc.stdout.readline()
  if not line:
    break

  ioerrmatched = None
  ioerrmatched = IOErrMatch.search( line )
  fmterrmatched = None
  fmterrmatched = InFmtErrMatch.search( line )
  parseerrmatched = None
  parseerrmatched = ParseErrMatch.search( line )
  rematched = None
  rematched = progressMatch.search( line )
  videndmatched = None
  videndmatched = endVideoStatsMatch.search( line )
  audendmatched = None
  audendmatched = endAudioStatsMatch.search( line )

  if ioerrmatched:
    print "I/O Error"
    break
  if fmterrmatched:
    print "Input Format not recognised"
    break
  if parseerrmatched:
    print "Command line parsing error"
    break
  if rematched:
    print "Whole == " + rematched.group(0)
    print "Pos == " + rematched.group(1)
    print "Frame == " + rematched.group(2)
    print "Percentage == " + rematched.group(3)
    print "FPS == " + rematched.group(4)
    print "RemMin == " + rematched.group(5)
    print "EstMB == " + rematched.group(6)
    print "VBR == " + rematched.group(7)
    print "ABR == " + rematched.group(8)
    pass
  if videndmatched:
    print "Whole == " + videndmatched.group(0)
    print "Video Kbit/s == " + videndmatched.group(1)
    print "Video Bytes == " + videndmatched.group(2)
    print "Video secs == " + videndmatched.group(3)
  if audendmatched:
    print "Whole == " + audendmatched.group(0)
    print "Audio Kbit/s == " + audendmatched.group(1)
    print "Audio Bytes == " + audendmatched.group(2)
    print "Audio secs == " + audendmatched.group(3)
  print "NOPARSE : " + line.rstrip()
