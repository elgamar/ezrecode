#!/usr/env python3

import subprocess
import shlex
import re
import sys
from threading import Thread
import queue

class MencoderWorker( Thread ):
        
    NOT_STARTED = "NOTSTARTED"
    RUNNING = "RUNNING"
    ERROR_IO = "IOERR"
    ERROR_FMT = "FMTERR"
    ERROR_PARSE = "PARSEERROR"
    IN_PROGRESS = "PROGRESS"
    DONE_VID_STATS = "VIDEOSTATS"
    DONE_AUD_STATS = "AUDIOSTATS"
    FINISHED = "FINISHED"
    
    def run( self ):
        ##DEBUG
        #print( "CMDSTRING -- ", self.__cmdString)
        #Mencoder process launch (subprocess.Popen object)
        try:
            self.__process = subprocess.Popen(shlex.split( self.__cmdString ),
                                              bufsize=0, stdout=subprocess.PIPE,
                                              stderr=subprocess.STDOUT,
                                              stdin=subprocess.PIPE,
                                              universal_newlines=True)
        except OSError:
            #TODO: add better error processing
            sys.stderr.write("[[ERROR: can't start mencoder process!]]\n")
            self.__outputBuffer.append("[[Mencoder process not started]]")
            self.__process = None            
        while True:
            (status, output, dictionary) = self.__getStatus()
            #print( status ) ##DEBUG
            self.__progressQueue.put( (status, output, dictionary) )
            if (status == MencoderWorker.FINISHED) or (status == MencoderWorker.NOT_STARTED):
                break
    
    def __init__(self, dialogParent, commandString, progressQueue ):
        Thread.__init__( self )
        
        #Communication queue with parent dialog's thread
        self.__progressQueue = progressQueue
        #Parent dialog used for user feedback
        self.__dialog = dialogParent
        #Mencoder command String to be executed
        self.__cmdString = str( commandString )
        #Mencoder output buffer for logging/reporting purposes
        self.__outputBuffer = []
        #Mencoder subprocess instance
        self.__process = None
        #TODO: parametrize them (reading from a file?)
        # Regular expression strings used to parse mencoder output
        self.__IOERR = r"^Cannot open file/device."
        self.__FMTERR = r"^============ Sorry, this file format is not recognized/supported ============="
        self.__PARSEERR = r"^Error parsing option on the command line:(.*?)"
        self.__PROGRESS = r"^Pos:\s*(\d+.\d?)s\s*(\d+?)f \(\s?(\d+)%\)\s*(\d+.\d+)fps Trem:\s*(\d+)min\s*(\d+)mb.*?A-V:.*?\[(\d+):(\d+)\](.*?)"
        self.__ENDVIDEOSTATS = r"^Video stream:\s*(\d+.\d+) kbit/s.*?size:\s*(\d+)\s*bytes\s*(\d+.\d+) secs.*?"
        self.__ENDAUDIOSTATS = r"^Audio stream:\s*(\d+.\d+) kbit/s.*?size:\s*(\d+)\s*bytes\s*(\d+.\d+) secs.*?"
        #Regexp compiling        
        ##Error handling
        self.__IOErrMatch = re.compile( self.__IOERR )
        self.__InFmtErrMatch = re.compile( self.__FMTERR )
        self.__ParseErrMatch = re.compile( self.__PARSEERR )
        ##Progress and final stats handling
        self.__progressMatch = re.compile( self.__PROGRESS )
        self.__endVideoStatsMatch = re.compile( self.__ENDVIDEOSTATS )
        self.__endAudioStatsMatch = re.compile( self.__ENDAUDIOSTATS )
        
    def terminateProcess(self):
        if self.__process:
            self.__process.terminate()
   
    def __getStatus(self):
        if self.__process: #Mencoder started. Let's see what's doing.            
            currLine = self.__process.stdout.readline()
            if not currLine: #No more lines to read: Mencoder finished
                self.__outputBuffer.append("\n")
                return(MencoderWorker.FINISHED,
                       self.__outputBuffer,
                       dict())           
            else: #Mencoder spitted out something: lets parse it
                ioerrmatched = None
                fmterrmatched = None
                parseerrmatched = None
                progressmatched = None
                videndmatched = None
                audendmatched = None
                ioerrmatched = self.__IOErrMatch.search( currLine )
                fmterrmatched = self.__InFmtErrMatch.search( currLine )
                parseerrmatched = self.__ParseErrMatch.search( currLine )
                progressmatched = self.__progressMatch.search( currLine )
                videndmatched = self.__endVideoStatsMatch.search( currLine )
                audendmatched = self.__endAudioStatsMatch.search( currLine )

                if ioerrmatched: #I/O Error
                    self.__outputBuffer.append( currLine.rstrip() )
                    return(MencoderWorker.ERROR_IO,
                           self.__outputBuffer,
                           dict())
                
                elif fmterrmatched: #Media format error
                    self.__outputBuffer.append( currLine.rstrip() )
                    return(MencoderWorker.ERROR_FMT,
                           self.__outputBuffer,
                           dict())
                
                elif parseerrmatched: #Command line parsing error
                    self.__outputBuffer.append( currLine.rstrip() )
                    return(MencoderWorker.ERROR_PARSE,
                           self.__outputBuffer,
                           dict())
                
                elif progressmatched:
                    self.__outputBuffer.append( currLine.rstrip() )
                    resultDict = {'POS' : progressmatched.group(1),
                                  'FRAME' : progressmatched.group(2),
                                  'PERCENT' : progressmatched.group(3),
                                  'FPS' : progressmatched.group(4),
                                  'REMTIME' : progressmatched.group(5),
                                  'ESTSIZE' : progressmatched.group(6),
                                  'VBR' : progressmatched.group(7),
                                  'ABR' : progressmatched.group(8)}
                    return(MencoderWorker.IN_PROGRESS, 
                            self.__outputBuffer, 
                            resultDict)
                
                elif videndmatched:
                    self.__outputBuffer.append( currLine.rstrip() )
                    resultDict = {'VBR' : videndmatched.group(1),
                                  'VSIZE' : videndmatched.group(2),
                                  'VTIME' : videndmatched.group(3)}
                    return(MencoderWorker.DONE_VID_STATS, 
                            self.__outputBuffer, 
                            resultDict)
                
                elif audendmatched:
                    self.__outputBuffer.append( currLine.rstrip() )
                    resultDict = {'ABR' : audendmatched.group(1),
                                  'ASIZE' : audendmatched.group(2),
                                  'ATIME' : audendmatched.group(3)}
                    return(MencoderWorker.DONE_AUD_STATS, 
                            self.__outputBuffer, 
                            resultDict)
                
                else: #Unparsed output: let's pass it through
                    self.__outputBuffer.append( currLine.rstrip() )
                    return( MencoderWorker.RUNNING,
                            self.__outputBuffer,
                            dict() ) 
        
        else: # Mencoder never started. Something it's wrong.
            return (MencoderWorker.NOT_STARTED,
                    self.__outputBuffer,
                    dict())

if (__name__ == "__main__"):
    #TODO: better unit testing
    mencoder = MencoderWorker( "mencoder input.avi -oac copy -ovc copy -o output.avi" )

