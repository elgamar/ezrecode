#!/usr/env python3

import os

#from Tkinter import *
#from ttk import *
#import tkFileDialog
from tkinter import *
from tkinter.ttk import *
import tkinter.filedialog

HELP_MSG = """
***ezRecode - Video Recoder***

* I/O options
------------
- Source video:     input video file
- Destination vide: output video file

* Format options
------------
- Domestic media player: target video format suitable for domestic media players (width=640px, DEFAULT)
- Portable media player: target video format suitable for portable players (width=320px)

* Video bitrate options
------------
- Automatic: ezRecode computes video bitrate automatically (DEFAULT)
- Manual:    ezRecode uses the specified video bitrate

* Audio bitrate options
------------
- Automatic: ezRecode computes audio bitrate automatically (DEFAULT)
- Manual:    ezRecode uses the specified audio bitrate

* Encoding speed
------------
Speed option varies from 1 to 5 - highest speed means lowest quality. (DEFAULT = 1)
"""

from progressDialog import EZRecodeProgressDialog

class EZRecodeApp( Frame ):

    def __showHelp(self):
        tkinter.messagebox.showinfo(title = "ezRecode usage",
                                    message = HELP_MSG)

    def __startConversion(self):
        progressDialog = Toplevel(self)
        progressDialogFrame = EZRecodeProgressDialog( master=progressDialog, cmdString=self.__getEncString() )
        
    def __callSrcFileOpen(self):
        inputName = tkinter.filedialog.askopenfilename()
        if ( inputName != "" ):
            self.SrcFileName.set( inputName )

    def __callDstFileOpen(self):
        outputName = tkinter.filedialog.asksaveasfilename()
        if ( outputName != "" ):
            self.DstFileName.set( outputName )

    def __computeVBR(self):
        #TODO: implement ffmpeg stdout parsing to determine bitrate
        if self.VBRVar.get() == "AUTO":
            if self.FmtVar.get() == "ZEN":
                self.VBRValue.set( 600 )
                return 600
            else:
                self.VBRValue.set( 1200 )
                return 1200
        else:
            return self.VBRValue.get()

    def __computeABR(self):
        #TODO: implement ffmpeg stdout parsing to determine bitrate
        if self.ABRVar.get() == "AUTO":
            if self.FmtVar.get() == "ZEN":
                self.ABRValue.set( 96 )
                return 96
            else:
                self.ABRValue.set( 192 )
                return 192
        else:
            return self.ABRValue.get()

    def __getEncString(self):
        #TODO: manage profiles and preferences - now just testing
        infileStr = "\"" + os.path.abspath( self.SrcFileName.get() ) + "\" "
        outfileStr = " -o \"" + os.path.abspath( self.DstFileName.get() ) + "\" "        
        if self.FmtVar.get() == "ZEN":
            videoWidth = 320
        else:
            videoWidth = 640
        filterStr = " -vf hqdn3d,softskip,scale=" + str(videoWidth) + ":-11,harddup -noskip "
        audFmtStr = " -oac mp3lame "
        vidFmtStr = " -ovc xvid "
        #TODO
        if self.SpeedValue.get() == 1:
            spdOpts = ":vhq=4:chroma_opt:hq_ac:trellis:quant_type=mpeg:threads=2"
        elif self.SpeedValue.get() == 2:
            spdOpts = ":vhq=3:trellis:quant_type=mpeg:threads=2"
        elif self.SpeedValue.get() == 3:
            spdOpts = ":vhq=2:trellis:threads=2"
        elif self.SpeedValue.get() == 4:
            spdOpts = ":vhq=1:threads=2"
        elif self.SpeedValue.get() == 5:
            spdOpts = ":vhq=0:threads=2:turbo"
        else: #Impossible!
            spdOpts = ""

        audEncOpts = " -lameopts vol=5:mode=0:q=0:cbr:br=" + str( self.__computeABR() )
        vidEncOpts = " -xvidencopts max_bframes=1:max_key_interval=250:bitrate=" + str( self.__computeVBR() ) + spdOpts + " " 
        
        encString = "mencoder " + infileStr + filterStr + audFmtStr + audEncOpts + vidFmtStr + vidEncOpts + " -ffourcc DX50 " + outfileStr 
        return( str( encString ) )

    def __createWidgets(self):

        ##Control variables setup
        self.SrcFileName = StringVar()
        self.DstFileName = StringVar()
        self.FmtVar = StringVar()
        self.VBRVar = StringVar()
        self.VBRValue = IntVar()
        self.ABRVar = StringVar()
        self.ABRValue = IntVar()
        self.SpeedValue = IntVar()

        ##Frames setup
        self.FileFrame = LabelFrame( master = self, text = "Video files" )
        self.SrcFrame = Frame( master = self.FileFrame )
        self.DstFrame = Frame( master = self.FileFrame )
        self.FmtFrame = LabelFrame( master = self, text = "Output Video Format")
        self.VBRFrame = LabelFrame( master = self, text = "Video Bitrate" )
        self.ABRFrame = LabelFrame( master = self, text = "Audio Bitrate" )
        self.SpeedFrame = LabelFrame( master = self, text = "Encoding speed (more speed => less quality)" )
        self.BtnFrame = Frame( master = self )

        #SrcFrame setup
        self.SrcLabel = Label( master = self.SrcFrame )
        self.SrcLabel["text"] = "Source video"
        self.SrcEntry = Entry( master = self.SrcFrame )
        self.SrcEntry["width"] = 50
        self.SrcEntry["textvariable"] = self.SrcFileName
        self.SrcFileName.set( "INPUT.AVI" )
        self.SrcButton = Button( master = self.SrcFrame )
        self.SrcButton["text"] = "..."
        self.SrcButton["command"] = self.__callSrcFileOpen
        #SrcFrame packing
        self.SrcLabel.pack( side = LEFT, fill = X, expand = YES, anchor = W )
        self.SrcEntry.pack( side = LEFT, fill = X, expand = YES )
        self.SrcButton.pack( side = LEFT, anchor = E )
        self.SrcFrame.pack( side = TOP, fill = X, expand = YES)

        #DstFrame setup
        self.DstLabel = Label( master = self.DstFrame )
        self.DstLabel["text"] = "Destination video"
        self.DstEntry = Entry( master = self.DstFrame )
        self.DstEntry["width"] = 50
        self.DstEntry["textvariable"] = self.DstFileName
        self.DstFileName.set( "OUTPUT.AVI" )
        self.DstButton = Button( master = self.DstFrame )
        self.DstButton["text"] = "..."
        self.DstButton["command"] = self.__callDstFileOpen
        #DstFrame packing
        self.DstLabel.pack( side = LEFT, fill = X, expand = YES, anchor = W  )
        self.DstEntry.pack( side = LEFT, fill = X, expand = YES )
        self.DstButton.pack( side = LEFT, anchor = E )
        self.DstFrame.pack( side = TOP, fill = X, expand = YES )
        ##File LabelFrame packing
        self.FileFrame.pack( side = TOP, fill = BOTH, expand = YES)

        #FmtFrame setup
        self.PALFmtOption = Radiobutton( master = self.FmtFrame, value = "PAL",
        text = "Domestic Media Player", variable = self.FmtVar )
        self.ZENFmtOption = Radiobutton( master = self.FmtFrame, value = "ZEN",
        text = "Portable Media Player", variable = self.FmtVar )
        self.FmtVar.set( "PAL" )
        #FmtFrame packing
        self.PALFmtOption.pack( side = LEFT, fill = X, expand = YES)
        self.ZENFmtOption.pack( side = LEFT, fill = X, expand = YES)
        self.FmtFrame.pack( side = TOP, fill = X, expand = YES )

        #VBRFrame setup
        self.VBRAutoOption = Radiobutton( master = self.VBRFrame, value = "AUTO",
        text = "Automatic   ", variable = self.VBRVar )
        self.VBRManualOption = Radiobutton( master = self.VBRFrame, value = "MANUAL",
        text = "Manual      ", variable = self.VBRVar )
        self.VBRVar.set( "AUTO" )
        self.VBRValueEntry = Entry( master = self.VBRFrame, width = 6, textvariable = self.VBRValue )
        self.VBRValue.set( 800 )
        #VBRFrame packing
        self.VBRAutoOption.pack( side = LEFT, expand = NO)
        self.VBRManualOption.pack( side = LEFT, expand = NO)
        self.VBRValueEntry.pack( side = LEFT, fill = X, expand = NO)
        self.VBRFrame.pack( side = TOP, fill = X, expand = YES )

        #ABRFrame setup
        self.ABRAutoOption = Radiobutton( master = self.ABRFrame, value = "AUTO",
        text = "Automatic   ", variable = self.ABRVar )
        self.ABRManualOption = Radiobutton( master = self.ABRFrame, value = "MANUAL",
        text = "Manual      ", variable = self.ABRVar )
        self.ABRVar.set( "AUTO" )
        self.ABRValueEntry = Entry( master = self.ABRFrame, width = 6, textvariable = self.ABRValue )
        self.ABRValue.set( 96 )
        #ABRFrame packing
        self.ABRAutoOption.pack( side = LEFT, expand = NO)
        self.ABRManualOption.pack( side = LEFT, expand = NO)
        self.ABRValueEntry.pack( side = LEFT, fill = X, expand = NO)
        self.ABRFrame.pack( side = TOP, fill = X, expand = YES )

        #SpeedFrame setup
        self.SpeedOption1 = Radiobutton( master = self.SpeedFrame, value = 1,
        text = "1 - Highest quality  ", variable = self.SpeedValue )
        self.SpeedOption2 = Radiobutton( master = self.SpeedFrame, value = 2,
        text = "2  ", variable = self.SpeedValue )
        self.SpeedOption3 = Radiobutton( master = self.SpeedFrame, value = 3,
        text = "3  ", variable = self.SpeedValue )
        self.SpeedOption4 = Radiobutton( master = self.SpeedFrame, value = 4,
        text = "4  ", variable = self.SpeedValue )
        self.SpeedOption5 = Radiobutton( master = self.SpeedFrame, value = 5,
        text = "5 - Lowest quality", variable = self.SpeedValue )
        self.SpeedValue.set( 1 )
        #SpeedFrame packing
        self.SpeedOption1.pack( side = LEFT, expand = NO)
        self.SpeedOption2.pack( side = LEFT, expand = NO)
        self.SpeedOption3.pack( side = LEFT, expand = NO)
        self.SpeedOption4.pack( side = LEFT, expand = NO)
        self.SpeedOption5.pack( side = LEFT, expand = NO)
        self.SpeedFrame.pack( side = TOP, fill = X, expand = YES )

        #BtnFrame setup
        self.QUIT = Button( master = self.BtnFrame )
        self.QUIT["text"] = "Quit"
        self.QUIT["command"] =  self.quit
        self.HELP = Button( master = self.BtnFrame )
        self.HELP["text"] = "Help"
        self.HELP["command"] = self.__showHelp
        self.CONVERT = Button( master = self.BtnFrame )
        self.CONVERT["text"] = "Start Video Conversion"
        self.CONVERT["command"] = self.__startConversion
        #BtnFrame packing
        self.QUIT.pack( side = LEFT )
        self.HELP.pack( side = LEFT )
        self.CONVERT.pack( side = LEFT )
        self.BtnFrame.pack( side = LEFT, expand = YES, fill = X)

    def __init__(self, master=None):
        #style = Style()
        #print( "DEBUG:", style.theme_use() )
        Frame.__init__(self, master)
        self.pack()
        self.__createWidgets()

if ( __name__ == "__main__" ):
    root = Tk()
    
    root.title("ezRecode 0.1")
    app = EZRecodeApp( master=root )
    app.mainloop()
    root.destroy()

