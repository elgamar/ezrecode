#!/usr/env python3

import threading
from tkinter import *
from tkinter.ttk import *
import tkinter.messagebox
import queue

from mencoderWorker import MencoderWorker

class EZRecodeProgressDialog(Frame):
        
    def __init__(self, master=None, cmdString="mencoder input.avi -oac copy -ovc copy -o output.avi" ):
        Frame.__init__(self, master)
        self.__mencoder = None
        self.__createWidgets()
        self.pack()
        self.__conversionOK = True
        self.__progressQueue = queue.Queue()        
        self.__startProcess( cmdString )
    
    def __startProcess(self, cmdString):
        self.__mencoder = MencoderWorker( self, cmdString, self.__progressQueue )
        self.__mencoder.start()
        while True:
            try:
                ( status, output, dictionary ) = self.__progressQueue.get( block=False )
                if status == MencoderWorker.FINISHED:
                    self.updateMencoderReport(status, output, dictionary)
                    break
                elif status == MencoderWorker.NOT_STARTED:
                    self.manageError(status, output, dictionary)
                    break
                elif status == MencoderWorker.ERROR_IO:
                    self.manageError(status, output, dictionary)
                elif status == MencoderWorker.ERROR_FMT:
                    self.manageError(status, output, dictionary)
                elif status == MencoderWorker.ERROR_PARSE:
                    self.manageError(status, output, dictionary)
                elif status == MencoderWorker.RUNNING:
                    self.updateProgressStatus(status, output, dictionary)
                elif status == MencoderWorker.IN_PROGRESS:
                    self.updateProgressStatus(status, output, dictionary)
                elif status == MencoderWorker.DONE_VID_STATS:
                    self.updateVideoStatistics(status, output, dictionary)
                elif status == MencoderWorker.DONE_AUD_STATS:
                    self.updateAudioStatistics(status, output, dictionary)
            except queue.Empty:
                pass

        
    def manageError(self, status, output, dictionary):
        #print("[[ERR - " + status + "]]" +  output.pop()) ## DEBUG- REMOVE ##        
        self.__conversionOK = False
        errString = "Unrecognized error." # == ERROR_PARSE
        if status == MencoderWorker.NOT_STARTED:
            errString = "Mencoder not found.\nInstall it and/or check your PATH."
        elif status == MencoderWorker.ERROR_IO:
            errString = "I/O error.\nInput file is not readable or output file is not writable."
        elif status == MencoderWorker.ERROR_FMT:
            errString = "Input format error.\nThe input video format is not recognized."

        tkinter.messagebox.showerror("Encoding error",
                                     "ERROR:\n" + errString)
        self.master.destroy()
    
    def updateProgressStatus(self, status, output, dictionary):
        #TODO: manage RUNNING, IN_PROGRESS
        if status == MencoderWorker.RUNNING:
            #TODO: add some Mencoder output dialog on users demand
            #print("[[RUN]]" +  output.pop()) ## DEBUG - REMOVE ##
            pass
        else:
            #print("[[PRO]]" +  output.pop()) ## DEBUG - REMOVE ##
            self.__ABR.set( dictionary["ABR"] )
            self.__VBR.set( dictionary["VBR"] )
            self.__FPS.set( dictionary["FPS"] )
            self.__PERCENTAGE.set( dictionary["PERCENT"] )
            self.__CURRPOS.set( dictionary["POS"] )
            self.__SIZE.set( dictionary["ESTSIZE"] )
            self.__REMAINING.set( dictionary["REMTIME"] )
            self.update()
    
    def updateAudioStatistics(self, status, output, dictionary):
        #Should be enough updating self.ABR
        self.__ABR.set( dictionary["ABR"] )
        self.update()
        #print("[[AUD]]" +  output.pop()) ## DEBUG - REMOVE ##

    def updateVideoStatistics(self, status, output, dictionary):
        #Should be enough updating self.VBR
        self.__VBR.set( dictionary["VBR"] )
        self.update()
        #print("[[VID]]" +  output.pop()) ## DEBUG - REMOVE ##

    def updateMencoderReport(self, status, output, dictionary):
        #print("[[END]]" +  output.pop()) ## DEBUG - REMOVE ##
        #TODO: add here final status dialog asking for confirmation with encoding report.
        #do it subclassing tkinter.simpledialog.Dialog
        #For now: quick and dirty solution
        if self.__conversionOK:
            tkinter.messagebox.showinfo("Encoding finished",
                                        "The encoding process terminated succesfully!")
        self.master.destroy()
        
    def __stopProcess(self):
        #TODO: improve confirmation dialog. Ok for now.
        if tkinter.messagebox.askokcancel(title = "Stop encoding", 
                                          message = "Stop the encoding process?"):
            if self.__mencoder:
                self.__conversionOK = False
                self.__mencoder.terminateProcess()
                self.master.destroy()

    def __createWidgets(self):
        
        #Status variables
        self.__ABR = IntVar()
        self.__VBR = IntVar()
        self.__CURRPOS = StringVar()
        self.__PERCENTAGE = IntVar()
        self.__REMAINING = StringVar()
        self.__FPS = StringVar()
        self.__SIZE = StringVar()
        
        #Layout frames
        self.__progressFrame = LabelFrame( master=self, text="Encoding progress" )
        self.__progressSubFrame = Frame(master = self.__progressFrame)
        self.__timeSubFrame = Frame(master = self.__progressFrame)        
        self.__statusFrame = LabelFrame( master=self, text="Encoding statistics" )
        self.__buttonFrame = Frame( master=self )
        
        #Progress Frame setup
        self.__encProgress = Progressbar( master=self.__progressFrame,
                                         orient=HORIZONTAL,
                                         length=400,
                                         mode='determinate',
                                         variable=self.__PERCENTAGE)
        self.__encProgress.pack( side = TOP, fill = X, expand = YES )
        
        self.__ProgressLabel = Label(master=self.__progressSubFrame,
                                    text="Encoding progress %: ")
        self.__ProgressLabel.pack( side = LEFT, fill = X, expand = NO, anchor = W )
        self.__ProgressEntryLbl = Label(master=self.__progressSubFrame,
                                       textvariable=self.__PERCENTAGE)
        self.__ProgressEntryLbl.pack( side = LEFT, fill = X, expand = NO, anchor = E )
        self.__progressSubFrame.pack( side = TOP, fill = X, expand = YES )

        self.__RemTimeLabel = Label(master=self.__timeSubFrame,
                                    text="Estimated remaining time (min.): ")
        self.__RemTimeLabel.pack( side = LEFT, fill = X, expand = NO, anchor = W )
        self.__RemTimeEntryLbl = Label(master=self.__timeSubFrame,
                                       textvariable=self.__REMAINING)
        self.__RemTimeEntryLbl.pack( side = LEFT, fill = X, expand = NO, anchor = E )
        self.__timeSubFrame.pack( side = TOP, fill = X, expand = YES )
        self.__progressFrame.pack( side = TOP, fill = X, expand = YES )
        
        #Status frame setup
        self.__VBRLabel = Label(master=self.__statusFrame,
                                text="Video Bitrate (kbit/s): ")
        self.__VBREntryLabel = Label(master=self.__statusFrame,
                                textvariable=self.__VBR)
        self.__ABRLabel = Label(master=self.__statusFrame,
                                text="Audio Bitrate (kbit/s): ")
        self.__ABREntryLabel = Label(master=self.__statusFrame,
                                textvariable=self.__ABR)
        self.__FPSLabel = Label(master=self.__statusFrame,
                                text="Speed (fps): ")
        self.__FPSEntryLabel = Label(master=self.__statusFrame,
                                textvariable=self.__FPS)
        self.__SIZELabel = Label(master=self.__statusFrame,
                                text="Estimated size (mb): ")
        self.__SIZEEntryLabel = Label(master=self.__statusFrame,
                                textvariable=self.__SIZE)
        self.__VBRLabel.grid( column=0, row=0, sticky=W )
        self.__VBREntryLabel.grid( column=1, row=0, sticky=E )
        self.__ABRLabel.grid( column=0, row=1, sticky=W )
        self.__ABREntryLabel.grid( column=1, row=1, sticky=E )
        self.__FPSLabel.grid( column=0, row=2, sticky=W )
        self.__FPSEntryLabel.grid( column=1, row=2, sticky=E )
        self.__SIZELabel.grid( column=0, row=3, sticky=W )
        self.__SIZEEntryLabel.grid( column=1, row=3, sticky=E )
        self.__statusFrame.pack( side = TOP, fill = BOTH, expand = YES)

        self.__stopBtn = Button(master=self.__buttonFrame,
                                text="Stop encoding",
                                command=self.__stopProcess)
        self.__stopBtn.pack( side=BOTTOM, anchor = CENTER )
        self.__buttonFrame.pack( side = TOP, fill = X, expand = YES )

if (__name__ == "__main__"):
    #TODO: unit testing
    pass

