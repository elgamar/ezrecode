#!/usr/env python3

#from distutils.core import setup
from cx_Freeze import setup, Executable
import sys

baseExe = None
if sys.platform == "win32":
    baseExe = "Win32GUI"

setup(name='ezRecode',
      version='0.1',
      description='ezRecode - video recoding tool',
      author='Gabriel Marcelli',
      author_email='gabriel.dot.marcelli at gmail.dot.com',
      package_dir={'': 'src'},
      packages=[''],
	  executables = [ Executable("src/ezRecode.py", base = baseExe) ],
	  includes = [ 'progressDialog', 'mencoderWorker' ]
      )
